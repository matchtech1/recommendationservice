﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RecommendationInfrastructure
{
    public class MongoManager
    {
        private MongoClient _mongoClient { get; set; }
        public readonly IMongoDatabase MongoDatabase;
        private readonly IMongoCollection<BsonDocument> _collection;

        public MongoManager(string databaseName)
        {
            string MongoClientConnectionString = "mongodb://127.0.0.1:27017";
            this._mongoClient = new MongoClient(MongoClientConnectionString);
            this.MongoDatabase = this._mongoClient.GetDatabase(databaseName);
            //this._collection = _database.GetCollection<BsonDocument>(collectionName);
        }

        //public async Task<T> FindByKey<T>(string key, string value)
        //{
        //    var filter = Builders<BsonDocument>.Filter.Eq(key, value);
        //    var document = await _collection.Find(filter).SingleOrDefaultAsync();

        //    return BsonSerializer.Deserialize<T>(document);
        //}

        //public async Task<T> UpdateByKey<T>(string key, string value)
        //{
        //    var filter = Builders<BsonDocument>.Filter.Eq(key, value);

        //    var update = Builders<BsonDocument>.Update.Set("authToken", authToken);
        //    await usersCollection.UpdateOneAsync(filter, update);

        //    return BsonSerializer.Deserialize<T>(document);
        //}
    }
}
