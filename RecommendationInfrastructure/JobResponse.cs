﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RecommendationInfrastructure
{
    public class JobResponse
    {
        // approve/reject
        public string ResponseStatus { get; set; }
        public DateTime Timestamp { get; set; }
    
        public JobResponse(string responseStatus)
        {
            ResponseStatus = responseStatus;
            Timestamp = DateTime.Now;
        }

        public JobResponse(string status,  DateTime start, DateTime end)
        {
            ResponseStatus = status;
            var randomTest = new Random();

            TimeSpan timeSpan = end - start;
            TimeSpan newSpan = new TimeSpan(0, randomTest.Next(0, (int)timeSpan.TotalMinutes), 0);
            DateTime newDate = start + newSpan;

            Timestamp = newDate;
        }
    }
}
