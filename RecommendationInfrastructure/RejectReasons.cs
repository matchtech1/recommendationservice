﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace RecommendationInfrastructure
{
    [JsonConverter(typeof(StringEnumConverter))]
    public enum RejectReasons
    {
        LOCATION,
        SKILLS,
        COMPANY,
        VIABILITY,
        ENVIRONMENT,
        OTHER
    }
}
