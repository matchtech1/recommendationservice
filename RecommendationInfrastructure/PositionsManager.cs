﻿using MongoDB.Bson;
using MongoDB.Driver;
using RecommendationInfrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace RecommendationInfrastructure
{
    public class PositionsManager
    {
        private static PositionsManager _instance;
        private readonly MongoManager _mongoManager;
        private IMongoCollection<Position> _positionsCollection;
        private IMongoCollection<UserModel> _usersCollection;
        private IMongoCollection<CandidateModel> _candidatesCollection;

        private const string DB_NAME = "MatchTech";
        private const string POSITION_COLLECTION_NAME = "Positions";
        private const string USERS_COLLECTION_NAME = "Users";

        public static PositionsManager GetInstance()
        {
            return _instance ??= new PositionsManager();
        }

        public PositionsManager()
        {
            _mongoManager = new MongoManager(DB_NAME);
            _positionsCollection = _mongoManager.MongoDatabase.GetCollection<Position>(POSITION_COLLECTION_NAME);
            _usersCollection = _mongoManager.MongoDatabase.GetCollection<UserModel>(USERS_COLLECTION_NAME);
            _candidatesCollection = _mongoManager.MongoDatabase.GetCollection<CandidateModel>(USERS_COLLECTION_NAME);
        }

        public Position GetRecommendation(string authToken)
        {
            var requestedCandidate = _candidatesCollection.Find(user => user.AuthToken.Equals(authToken)).SingleOrDefault();
            return _positionsCollection
                .Find(position => position.Id.Equals(requestedCandidate.CurrentRecommendation))
                .SingleOrDefault();
        }

        public Position ProceedReccomendation(string authToken)
        {
            var candidate = _candidatesCollection.Find(user => user.AuthToken.Equals(authToken)).SingleOrDefault();
            if (candidate != null)
            {
                var positionsToCheck = _positionsCollection.Find(position =>
                    !candidate.ViewedPositions.Contains(position.Id) &&
                    !position.Id.Equals(candidate.CurrentRecommendation)).ToList();
                var nextPosition = positionsToCheck.FirstOrDefault(position =>
                    position.RequiredSkills.All(skill => candidate.Skills.Contains(skill)));

                AddToViewedPositions(authToken, candidate.CurrentRecommendation);
                UpdateCurrentRecommendation(authToken, nextPosition?.Id);
                return nextPosition;
            }

            return null;
        }

        public void AddToViewedPositions(string authToken, string positionId)
        {
            if (string.IsNullOrEmpty(positionId)) return;
            var filter = Builders<UserModel>.Filter.Eq(user => user.AuthToken, authToken);
            var update = Builders<UserModel>.Update.Push(user => ((CandidateModel)user).ViewedPositions, positionId);
            _usersCollection.UpdateOne(filter, update);
        }

        public void UpdateCurrentRecommendation(string authToken, string currentRecommendation)
        {
            var filter = Builders<UserModel>.Filter.Eq(user => user.AuthToken, authToken);
            var update = Builders<UserModel>.Update.Set(user => ((CandidateModel)user).CurrentRecommendation, currentRecommendation);
            _usersCollection.UpdateOne(filter, update);
        }

        public Position GetNextReccomendation(string authToken)
        {
            var requestedUser = _candidatesCollection.Find(user => user.AuthToken.Equals(authToken)).SingleOrDefault();
            if (requestedUser != null)
            {
                var positionsToCheck = _positionsCollection.Find(position =>
                    !requestedUser.ViewedPositions.Contains(position.Id) &&
                    !position.Id.Equals(requestedUser.CurrentRecommendation)).ToList();
                return positionsToCheck.FirstOrDefault(position =>
                    position.RequiredSkills.All(skill => requestedUser.Skills.Contains(skill)));
            }
            return null;
        }
        public void InitRecommendations(string authToken)
        {
            var filter = Builders<UserModel>.Filter.Eq(user => user.AuthToken, authToken);
            var requestedCandidate = (CandidateModel)_usersCollection.Find(filter).SingleOrDefault();
            requestedCandidate.ViewedPositions = new List<string>();
            var update = Builders<UserModel>.Update.Set(user => ((CandidateModel)user).ViewedPositions, requestedCandidate.ViewedPositions);
            _usersCollection.UpdateOne(filter, update);
        }

        public async Task AddPosition(string authToken, Position position)
        {
            string userId = await GetUserIdByAuthToken(authToken);
            position.RecuiterId = userId;
            _positionsCollection.InsertOne(position);
        }

        public async Task<Position> GetPosition(string authToken, string positionId)
        {
            string recuiterId = await GetUserIdByAuthToken(authToken);
            var filter = Builders<Position>.Filter.And(
                Builders<Position>.Filter.Eq(position => position.Id, positionId),
                Builders<Position>.Filter.Eq(position => position.RecuiterId, recuiterId));
            return await _positionsCollection.Find(filter).SingleOrDefaultAsync();
        }

        public async Task<IEnumerable<Position>> GetPositions(string authToken)
        {
            string recuiterId = await GetUserIdByAuthToken(authToken);
            var filter = Builders<Position>.Filter.Eq(position => position.RecuiterId, recuiterId);
            return await _positionsCollection.Find(filter).ToListAsync();
        }

        public async Task<bool> IsCandidate(string authToken)
        {
            var filter = Builders<CandidateModel>.Filter.Eq(user => user.AuthToken, authToken);
            var user = await _candidatesCollection.Find(filter).SingleOrDefaultAsync();
            if (user == null) return false;
            return !user.IsRecuiter;
        }

        public async Task<string> GetUserIdByAuthToken(string authToken)
        {
            var filter = Builders<UserModel>.Filter.Eq(user => user.AuthToken, authToken);
            var user = await _usersCollection.Find(filter).SingleOrDefaultAsync();
            if (user == null)
            {
                return null;
            }
            return user.Id;
        }
    }
}
