﻿using MongoDB.Driver;
using RecommendationInfrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using static MongoDB.Driver.WriteConcern;

namespace RecommendationInfrastructure
{
    public class StatisticsManager
    {
        private static StatisticsManager _instance;
        private readonly MongoManager _mongoManager;
        private IMongoCollection<PositionStatistics> _statisticsCollection;
        private IMongoCollection<CandidateModel> _candidateCollection;

        private const string DB_NAME = "MatchTech";
        private const string COLLECTION_NAME = "PositionsStatistics";
        private const string USERS_COLLECTION_NAME = "Users";


        public StatisticsManager()
        {
            _mongoManager = new MongoManager(DB_NAME);
            _statisticsCollection = _mongoManager.MongoDatabase.GetCollection<PositionStatistics>(COLLECTION_NAME);
            _candidateCollection = _mongoManager.MongoDatabase.GetCollection<CandidateModel>(USERS_COLLECTION_NAME);
        }

        public static StatisticsManager GetInstance()
        {
            return _instance ??= new StatisticsManager();
        }

        public async Task<IEnumerable<RejectReason>> GetPositionRejectReasons(string positionId)
        {
            var positionStatics = await GetPositionStatisticsById(positionId);
            var rejectReasons = new List<RejectReason>();
            positionStatics.RejectReasons.ToList().ForEach(reason => rejectReasons.Add(new RejectReason(reason)));
            return rejectReasons;
        }

        public async Task<PositionStatistics> GetPositionStatisticsById(string positionId)
        {
            return await _statisticsCollection.Find(positionStatistics => positionStatistics.PositionId.Equals(positionId)).FirstOrDefaultAsync();
        }

        public async Task UpdateRejectReason(string positionId, string rejectReason)
        {
            Enum.TryParse(rejectReason, out RejectReasons rejectReasonEnum);
            var filter = Builders<PositionStatistics>.Filter.Eq(position => position.PositionId, positionId);
            var update = Builders<PositionStatistics>.Update
                .Inc(position => position.NumOfRejects, 1).Inc(position => position.RejectOptionsCountDict[rejectReasonEnum.ToString()], 1)
                 .Push(position => position.JobResponses, new JobResponse("Reject"))
                .Push(position => position.RejectReasons, rejectReasonEnum.ToString());


            var result = await _statisticsCollection.UpdateOneAsync(filter, update);
            if (result.ModifiedCount == 0)
            {
                AddRejectPosition(positionId, rejectReason);
            }
        }

        public async Task UpdateApprovePosition(string positionId, string authToken)
        {
            var candidate = _candidateCollection.Find(user => user.AuthToken.Equals(authToken)).SingleOrDefault();
            if (candidate != null) 
            { 
                var filter = Builders<PositionStatistics>.Filter.Eq(position => position.PositionId, positionId);
                var update = Builders<PositionStatistics>.Update.Inc(position => position.NumOfAccepts, 1).
                    Inc(position => position.ExprianceCountDict[GetExprianceYearsType(candidate.ExprianceYears).ToString()], 1)
                    .Push(position => position.JobResponses, new JobResponse("Approve"));
                var result = await _statisticsCollection.UpdateOneAsync(filter, update);
                if (result.ModifiedCount == 0)
                {
                    AddApprovedPosition(positionId, candidate.ExprianceYears);
                }
            }
        }

        public void AddApprovedPosition(string positionId, int exprianceYears)
        {
            var newPosition = new PositionStatistics()
            {
                NumOfAccepts = 1,
                NumOfRejects = 0,
                PositionId = positionId,
                RejectReasons = new List<string>(),
                RejectOptionsCountDict = new Dictionary<string, int>(),
                ExprianceCountDict = new Dictionary<string, int>()
                {
                    [GetExprianceYearsType(exprianceYears).ToString()] = 1
                },
                JobResponses = new List<JobResponse>() { new JobResponse("Approve")}
            };
            _statisticsCollection.InsertOne(newPosition);
        }

        public void AddRejectPosition(string positionId, string rejectReason)
        {
            var newPosition = new PositionStatistics()
            {
                NumOfAccepts = 0,
                NumOfRejects = 1,
                PositionId = positionId,
                RejectReasons = new List<string>()
                {
                    rejectReason.ToString()
                },
                RejectOptionsCountDict = new Dictionary<string, int>()
                {
                    [rejectReason.ToString()] = 1
                },
                ExprianceCountDict = new Dictionary<string, int>(),
                JobResponses = new List<JobResponse>() { new JobResponse("Reject") }
            };
            _statisticsCollection.InsertOne(newPosition);
        }

        public Dictionary<RejectReasons, int> UpdateCountDictionary(Dictionary<RejectReasons, int> reasonsCountDict, RejectReasons rejectReason)
        {
            if (reasonsCountDict.ContainsKey(rejectReason))
            {
                reasonsCountDict[rejectReason]++;
            }
            else
            {
                reasonsCountDict.Add(rejectReason, 1);
            }
            return reasonsCountDict;
        }

        public ExprianceYearsEnum GetExprianceYearsType(int exprianceYears)
        {
            if (exprianceYears <= 2)
            {
                return ExprianceYearsEnum.Junior;
            }

            if (exprianceYears <= 5) 
            {
                return ExprianceYearsEnum.Mid;
            }

            return ExprianceYearsEnum.Senior;
        }

        public async Task<List<Dictionary<string, int>>> GetWeeklyPositionResponseAsync(string positionId)
        {
            var positionStatics = await GetPositionStatisticsById(positionId);
            if (positionStatics != null)
            {
                var approvedDict = new Dictionary<string, int>();
                var rejectedDict = new Dictionary<string, int>();
                DateTime today = DateTime.Now.Date;
                DateTime oneWeekAgo = today.AddDays(-7);

                try
                {
                    foreach (var item in positionStatics.JobResponses)
                    {
                        if (item.Timestamp >= oneWeekAgo && item.Timestamp <= today)
                        {
                            string day = item.Timestamp.DayOfWeek.ToString();
                            if (item.ResponseStatus.Equals("Approve"))
                            {
                                if (approvedDict.ContainsKey(day))
                                    approvedDict[day] += 1;
                                else
                                    approvedDict[day] = 1;
                            }
                            else
                            {
                                if (rejectedDict.ContainsKey(day))
                                    rejectedDict[day] += 1; 
                                else
                                    rejectedDict[day] = 1;
                            }
                        }
                    }
                }

                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
                return new List<Dictionary<string, int>> { approvedDict, rejectedDict };
            }
            return null;
        }
    }
}
