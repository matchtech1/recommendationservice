﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace RecommendationInfrastructure.Models
{
    [BsonDiscriminator(Required = true)]
    [BsonKnownTypes(typeof(RecruiterModel), typeof(CandidateModel))]
    public class UserModel
    {
        [BsonId]
        [BsonIgnoreIfDefault]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        public string Email { get; set; }
        public string FullName { get; set; }
        public string Password { get; set; }
        public string AuthToken { get; set; }
        public bool IsRecuiter { get; set; }
    }
}
