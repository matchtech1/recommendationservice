﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace RecommendationInfrastructure.Models
{

    public class Position
    {
        [BsonId]
        [BsonIgnoreIfDefault]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        public string RecuiterId { get; set; }
        public string CompanyName { get; set; }
        public string PositionName { get; set; }
        public string Details { get; set; }
        public IEnumerable<string> RequiredSkills { get; set; }

        public int RequiredExprianceYears { get; set; }
        public string CompanyInfo { get; set; }
        public string Location { get; set; }
        public CompanySize CompanySize { get; set; }

        public string EducationLevel { get; set; }
    }
}
