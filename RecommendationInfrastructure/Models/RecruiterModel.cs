﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RecommendationInfrastructure.Models
{
    public class RecruiterModel: UserModel
    {
        public RecruiterModel() 
        { 
            this.IsRecuiter = true;
        }
    }
}
