﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RecommendationInfrastructure.Models
{
    public class CandidateModel : UserModel
    {
        public CandidateModel()
        {
            this.IsRecuiter = false;
            this.ViewedPositions = new List<string>();
            this.Skills = new List<string>();
        }

        public int MinSalary { get; set; }
        public int MaxSalary { get; set; }
        public int ExprianceYears { get; set; }
        public IEnumerable<string> Skills { get; set; }
        public IEnumerable<string> ViewedPositions { get; set; }
        public string CurrentRecommendation { get; set; }
        public string EducationLevel { get; set; }
    }
}
