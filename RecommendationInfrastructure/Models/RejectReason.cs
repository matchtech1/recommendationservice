﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace RecommendationInfrastructure.Models
{
    public class RejectReason
    {
        public DateTime Time { get; set; }
        public string Description { get; set; }

        public RejectReason(string description)
        {
            Random rnd = new Random();
            Description = description;
            TimeSpan timeSpan = DateTime.Now - DateTime.Now.AddDays(-7);
            TimeSpan newSpan = new TimeSpan(0, rnd.Next(0, (int)timeSpan.TotalMinutes), 0);
            Time = DateTime.Now.AddDays(-7) + newSpan;
        }
    }
}
