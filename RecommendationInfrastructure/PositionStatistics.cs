﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace RecommendationInfrastructure
{
    public class PositionStatistics
    {
        [BsonId]
        [BsonIgnoreIfDefault]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        public int NumOfAccepts { get; set; }
        public int NumOfRejects { get; set; }
        public string PositionId { get; set; }
        public IEnumerable<string> RejectReasons { get; set; }
        public Dictionary<string, int> RejectOptionsCountDict { get; set; }

        public Dictionary<string, int> ExprianceCountDict {get; set;}

        public List<JobResponse> JobResponses { get; set; }
    }
}
