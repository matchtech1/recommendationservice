﻿using MongoDB.Bson;
using RecommendationInfrastructure;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace RecommendationService.Model
{
    public class RejectPositionDto
    {
        [Required]
        public string PositionId { get; set; }
        [Required]
        public string RejectReason { get; set; }
    }
}
