﻿using System;

namespace RecommendationService.Model
{
    public class RejectReasonDto
    {
        public DateTime Time { get; set; }
        public string Description { get; set; }
    }
}
