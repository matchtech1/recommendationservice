﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MongoDB.Driver;
using RecommendationInfrastructure;
using RecommendationInfrastructure.Models;
using RecommendationService.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RecommendationService.Controllers
{
    [EnableCors("AllowSpecificOrigin")]
    [ApiController]
    [Route("[controller]")]
    public class RecommendationController : ControllerBase
    {
        private readonly ILogger<RecommendationController> _logger;
        private readonly PositionsManager _recommendationsManager;
        private IMongoCollection<CandidateModel> _candidatesCollection;

        public RecommendationController(ILogger<RecommendationController> logger, PositionsManager recommendationsManager)
        {
            _recommendationsManager = recommendationsManager;
            _logger = logger;
        }

        [HttpGet("current/{authToken}")]
        public async Task<ActionResult<Position>> GetReccomendation(string authToken)
        {
            if (!await _recommendationsManager.IsCandidate(authToken))
            {
                return Unauthorized();
            }
            var recommendation = _recommendationsManager.GetRecommendation(authToken);
            if (recommendation != null)
            {
                return Ok(recommendation);
            }
            return Ok(_recommendationsManager.ProceedReccomendation(authToken));
        }

        [HttpGet("next/{authToken}")]
        public async Task<ActionResult<Position>> GetNextReccomendation(string authToken)
        {
            if (!await _recommendationsManager.IsCandidate(authToken))
            {
                return Unauthorized();
            }
            return Ok(_recommendationsManager.GetNextReccomendation(authToken));
        }

        [HttpPost("{authToken}")]
        public async Task<ActionResult<Position>> ProceedReccomendation(string authToken)
        {
            if (!await _recommendationsManager.IsCandidate(authToken))
            {
                return Unauthorized();
            }
            return Ok(_recommendationsManager.ProceedReccomendation(authToken));
        }

        [HttpPost("init/{authToken}")]
        public async Task<ActionResult> InitReccomendations(string authToken)
        {
            if (!await _recommendationsManager.IsCandidate(authToken))
            {
                return Unauthorized();
            }
            _recommendationsManager.InitRecommendations(authToken);
            return Ok();
        }
    }
}