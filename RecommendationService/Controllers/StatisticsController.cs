using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using RecommendationInfrastructure;
using RecommendationInfrastructure.Models;
using RecommendationService.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RecommendationService.Controllers
{
    [EnableCors("AllowSpecificOrigin")]
    [ApiController]
    [Route("[controller]")]
    public class StatisticsController : Controller
    {
        private readonly ILogger<StatisticsController> _logger;
        private readonly StatisticsManager _statisticsManager;

        public StatisticsController(ILogger<StatisticsController> logger, StatisticsManager statisticsManager)
        {
            _statisticsManager = statisticsManager;
            _logger = logger;
        }

        [HttpGet("{positionId}")]
        public async Task<ActionResult<PositionStatistics>> GetPositionStatistics(string positionId)
        {
            var positionStatistics = await _statisticsManager.GetPositionStatisticsById(positionId);
            if (positionStatistics != null)
                return Ok(positionStatistics);
            return NoContent();
        }

        [HttpGet("Reject/{authToken}/{positionId}")]
        public async Task<ActionResult<IEnumerable<RejectReason>>> GetRejectReasons(string authToken, string positionId)
        {
            var responses = await _statisticsManager.GetPositionRejectReasons(positionId);
            return Ok(responses);
        }

        //TODO - remove it when the db is full
        public IEnumerable<RejectReasonDto> GetRejectReasonsList()
        {
            Random random = new Random();
            List<string> reasonsForRejectingJob = new List<string>()
            {
                "Poor salary offered compared to expectations",
                "Long commute distance",
                "Lack of growth opportunities",
                "Company's poor reputation",
                "Unfavorable work schedule",
                "No work from home option",
                "Inadequate benefits package",
                "Unprofessional interviewer behavior",
                "Role not aligned with skills and experience",
                "Negative Glassdoor reviews",
                "Unpleasant interview experience",
                "Unrealistic job expectations",
                "Concerns about job security",
                "Difficult or toxic work environment",
                "Poor company culture",
                "Better offer from another company",
                "Insufficient opportunities for creativity",
                "Job description was unclear or misleading",
                "Limited opportunities for training and development",
                "Unimpressive office location or facilities",
                "Lack of interest or passion for the company's mission or product",
                "Unreasonable travel requirements",
                "Unrealistic workload expectations",
                "Lack of diversity and inclusion",
                "Poor work-life balance",
                "Lack of autonomy and control",
                "Lack of alignment with personal values",
                "Inflexible or rigid company policies",
                "No opportunities for advancement or promotion",
                "Poor management or leadership",
            };

            for (int i = 0; i < 100; i++)
            {
                yield return new RejectReasonDto() { Time = DateTime.Now.AddMinutes(i * 17), Description = reasonsForRejectingJob[random.Next(30)] };
            }
        }

        [HttpPost("Reject/{authToken}")]
        public async Task<ActionResult> RejectPosition(string authToken, [FromBody] RejectPositionDto rejectPosition)
        {
            await _statisticsManager.UpdateRejectReason(rejectPosition.PositionId, rejectPosition.RejectReason);
            return Ok();
        }

        [HttpPost("Approve/{positionId}/{authToken}")]
        public async Task<ActionResult> UpdateApprovePosition(string authToken, string positionId)
        {
            await _statisticsManager.UpdateApprovePosition(positionId, authToken);
            return Ok();
        }

        [HttpGet("weeklyStatistics/{positionId}")]
        public async Task<ActionResult<IEnumerable<Dictionary<string, int>>>> GetPositionWeeklyStatistic(string positionId)
        {
            var weeklyStats = await _statisticsManager.GetWeeklyPositionResponseAsync(positionId);
            return Ok(weeklyStats);
        }
    }
}
