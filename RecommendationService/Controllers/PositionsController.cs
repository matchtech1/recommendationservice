﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using RecommendationInfrastructure;
using RecommendationInfrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RecommendationService.Controllers
{
    [EnableCors("AllowSpecificOrigin")]
    [ApiController]
    [Route("[controller]")]
    public class PositionsController : Controller
    {
        private readonly ILogger<PositionsController> _logger;
        private readonly PositionsManager _positionsManager;

        public PositionsController(ILogger<PositionsController> logger, PositionsManager positionsManager)
        {
            _positionsManager = positionsManager;
            _logger = logger;
        }

        [HttpGet("{authToken}")]
        public async Task<IEnumerable<Position>> GetPositions(string authToken)
        {
            return await _positionsManager.GetPositions(authToken);
        }

        [HttpGet("{authToken}/{positionId}")]
        public async Task<ActionResult<Position>> GetPosition(string authToken, string positionId)
        {
            return await _positionsManager.GetPosition(authToken, positionId);
        }
       

        [HttpPost("{authToken}")]
        public async Task AddPosition([FromBody]Position position, string authToken)
        {
            await _positionsManager.AddPosition(authToken, position);
        }
    }
}
